/*Нужно написать программку на любом языке (предпочтительны: с/с++, js, lua, python, java, bash).
1. Программа запрашивает у пользователя число N. Затем генерирует строку из случайных символов длиной N. Случайные символы в данном случае - это любые символы латинского алфавита
(A-Z) и цифры (0-9). Сгенерированная строка выводится на экран.
2. Далее программа запрашивает у пользователя один символ. Заменяет все буквы в сгенерированной на шаге 1 строке на этот символ.
3. Запрашивает второй символ. Заменяет все цифры в сгенерированной на шаге 2 строке на второй символ. Выводит получившуюся строку.
4. Выводит количество повторов первого символа и количество повторов второго символа.
Замечание: замену реализовать через цикл for, не через replace или подобные функции.*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int lenStr=0;//длинна строки
    int cnt=0;//основной счетчик
    int cntSym1=0;//счетчик укв
    int cntSym2=0;//счетчик цифр
    char symb;//символ
    char* randStr=0;//указатель на строку

    printf("Enter a length of string\n");//dвод длинны строки
    scanf("%d",&lenStr);

    randStr=(char*)malloc((lenStr+1)*sizeof(char));//выделение памяти + символ конца строки


    srand(time(NULL));//для генератора

    while(lenStr!=cnt)
    {
        symb=rand()%123;//случайное значение

        switch (symb)
        {
        case 48 ... 57://диапазон цифр
            randStr[cnt++]=symb;
            break;

        case 65 ... 90://диапазон строчных букв
            randStr[cnt++]=symb;
            break;

        case 97 ... 122://диапазон прописных букв
            randStr[cnt++]=symb;
            break;
        }
    }

    randStr[lenStr]='\0';

    for(int i=0; *(randStr+i)!='\0'; i++)//вывод строки
    {
        printf("%c",*(randStr+i));
    }

    printf("\n");
    fflush(stdin);//очистка буфера входногопотока

    printf("Enter any symbol number one\n");
    scanf("%c",&symb);

    for(int i=0; *(randStr+i)!='\0'; i++)//замена букв
    {
        switch (*(randStr+i))
        {
        case 65 ... 90:
            *(randStr+i)=symb;
            cntSym1++;
            break;

        case 97 ... 122:
            *(randStr+i)=symb;
            cntSym1++;
            break;
        }

        printf("%c",*(randStr+i));
    }

    printf("\n");
    fflush(stdin);

    printf("Enter any symbol number two\n");//замена цифер
    scanf("%c",&symb);

    for(int i=0; *(randStr+i)!='\0'; i++)
    {
        switch (*(randStr+i))
        {
        case 48 ... 57:
            *(randStr+i)=symb;
            cntSym2++;
            break;
        }

        printf("%c",*(randStr+i));
    }

    printf("\n");

    printf("Count of symbols number one = %d\n",cntSym1);
    printf("Count of symbols number two = %d",cntSym2);

    free(randStr);//очистка памяти

    return 0;
}
